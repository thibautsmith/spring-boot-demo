FROM maven:3-openjdk-17
WORKDIR /java/src/
COPY . ./
RUN mvn clean package -e


FROM openjdk:17-jdk-alpine
COPY --from=0 /java/src/target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]
